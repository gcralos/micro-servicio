package com.carlos.perfiles;

import com.carlos.perfiles.dto.Profile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.MimeTypeUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(value = "SpringBootTest.WebEnvironment.MOCK,classes = PerfilesApplication.class")
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:bootstrap-test.properties")
public class PerfilServicesIn {

    private static LocalDate localDate;
    private static Profile profile;

    @Autowired
    private MockMvc mockMvc;


    @BeforeClass
    public  static void setUp() {
        localDate = LocalDate.now();
        profile = new Profile();
    }

    @Test
    public void addProfileTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/v1/api/profile")
                .content(profileJson(1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated()).andReturn();
    }

    @Test
    public void findAllTest() throws Exception {
        this.addProfileTest();
        final ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders.get("/v1/api/profile")
                        .accept(MimeTypeUtils.APPLICATION_JSON_VALUE));
        resultActions.andExpect(status().isOk());
        resultActions.andExpect((jsonPath("$[0].id").value(1)));
    }

    @Test
    public void findByIdTest() throws Exception {

        final ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders.get("/v1/api/profile/{profileId}", 1)
                        .accept(MimeTypeUtils.APPLICATION_JSON_VALUE));
        resultActions.andExpect(status().isOk());
        resultActions.andExpect((jsonPath("$.id").value(1)));
    }

    @Test
    public void updateTest() throws Exception {
        this.addProfileTest();
        final ResultActions resultActions = this.mockMvc
                .perform(MockMvcRequestBuilders.put("/v1/api/profile/{profileId}", 1)
                        .content(profileJson(2))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));
        resultActions.andExpect(status().isOk());


    }

    @Test
    public void deleteTest() throws Exception {
        this.mockMvc.perform( MockMvcRequestBuilders.delete("/v1/api/profile/{profileId}", 1) )
                .andExpect(status().isOk());
    }

    private String profileJson( int  id) throws JsonProcessingException {

        profile.setId(id);
        profile.setNombre("Carlos Arturo");
        profile.setApellido("Gonzalez Alvarez");
        profile.setDireccion("crra 97  # 1-209");
        profile.setTelefono("317 8851461");
        profile.setActive(true);
        profile.setCreateBy("Carlos arturo  gonzalez");
        profile.setCreateDt(localDate.format(DateTimeFormatter.BASIC_ISO_DATE));
        profile.setApdateBy("Carlos arturo  gonzalez");
        profile.setUpdateDt(localDate.format(DateTimeFormatter.BASIC_ISO_DATE));

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String perfilJson = ow.writeValueAsString(profile);

        return perfilJson;
    }
}
