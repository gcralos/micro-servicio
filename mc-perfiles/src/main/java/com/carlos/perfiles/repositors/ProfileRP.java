package com.carlos.perfiles.repositors;

import com.carlos.perfiles.entitys.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProfileRP extends JpaRepository<ProfileEntity, Integer> {
     Optional<ProfileEntity> findById(int id);
}
