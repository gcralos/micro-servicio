package com.carlos.perfiles.services;


import com.carlos.perfiles.entitys.ProfileEntity;
import com.carlos.perfiles.repositors.ProfileRP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfileServices {
     @Autowired
     private ProfileRP profileRP;

    public List<ProfileEntity> findAll(){
         return   profileRP.findAll();
     }
     public  ProfileEntity findById( int id){
         Optional<ProfileEntity> optional= profileRP.findById(id);
         return optional.get();
     }
     public ResponseEntity<Object>  add( ProfileEntity profile){
         ProfileEntity profileEntity = profileRP.save(profile);
            return  ResponseEntity.status(201).body("usuario  ingrasado  con exito");
     }

     public  ResponseEntity<Object>  update(ProfileEntity  profile, int  id){
                Optional<ProfileEntity> optional = profileRP.findById(id);
                profile.setId(id);
                profileRP.save(profile);
         return  ResponseEntity.status(200).body("Perfil  actualizado con exito");
     }

     public  void  delete(int id){
         profileRP.deleteById(id);
     }

}
