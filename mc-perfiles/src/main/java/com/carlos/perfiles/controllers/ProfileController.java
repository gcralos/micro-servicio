package com.carlos.perfiles.controllers;

import com.carlos.perfiles.dto.Profile;
import com.carlos.perfiles.entitys.ProfileEntity;
import com.carlos.perfiles.services.ProfileServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/api")
public class ProfileController {

     @Autowired
     private ProfileServices profileServices;

     private ModelMapper  modelMapper = new ModelMapper();

     @GetMapping("/perfiles")
     private List<ProfileEntity>  getUsers(){
         //Profile profile = modelMapper.map(,Profile.class);
         return profileServices.findAll();
     }
     @GetMapping("/perfiles/{profielId}")
     private Profile  getProfile(@PathVariable  int profielId){
         Profile profile = modelMapper.map(profileServices.findById( profielId ),Profile.class);
         return profile ;
     }
     @PostMapping("/perfiles")
     private ResponseEntity<Object> addProFile(@RequestBody Profile profile ){
         ProfileEntity  profileEntity = modelMapper.map(profile, ProfileEntity.class
         );
          return profileServices.add(profileEntity);
     }

     @PutMapping("/perfiles/{profileId}")
     private  ResponseEntity<Object>  updateProfile( @RequestBody Profile profile, @PathVariable   int profileId){
         ProfileEntity  profileEntity = modelMapper.map(profile,ProfileEntity.class);
         return  profileServices.update(profileEntity,profileId);
     }

     @DeleteMapping("/perfiles/{profielId}")
     private  void   delete( @PathVariable  int profielId){
           profileServices.delete(profielId);
     }
}
