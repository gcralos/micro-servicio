package com.carlos.perfiles.entitys;

import javax.persistence.*;

@Entity
@Table(name="sys_datos_localizacion_profecional")
public class DatosLocalizacionProfecional {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private  Long  id;
    @Column
    private  String  nombreEmpresa;
    @Column
    private  String  cargo;
    @Column
    private  String  rol;
    @Column
    private  boolean  active;
    @Column
    private  String createBy;
    @Column
    private String  createDt;
    @Column
    private  String  apdateBy;
    @Column
    private String  updateDt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getApdateBy() {
        return apdateBy;
    }

    public void setApdateBy(String apdateBy) {
        this.apdateBy = apdateBy;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }
}
