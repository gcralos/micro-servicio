package com.carlos.perfiles.entitys;

import com.netflix.discovery.provider.ISerializer;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="sys_datos_localizacion")
public class DatosLocalizacionEntity  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private   Long  id;

    @Column(name="codigo_postal")
    private   String  codigoPostal;

    @Column(name="localida",length = 50)
    private String  localida;

    @Column(name="nombre_via",length = 50)
    private String  provincia;

    @Column(name="tipo_via",length = 50)
    private String  tipoVia;

    @Column(name="nombre_via",length = 50)
    private String  nombreVia;

    @Column(name="nuemero_via",length =5)
    private int  numeroVia;

    @Column(length = 50)
    private String  portal;

    @Column(length = 10)
    private String  bloque;

    @Column(length = 5)
    private String  piso;

    @Column(length = 4)
    private String  puerta;

    @Column(length = 4)
    private String  telefono;

    @Column(length = 4)
    private String  movil;

    @Column(length = 4)
    private String  fax;

    @Column
    private  String  iban;
    @Column
    private  boolean  active;
    @Column
    private  String createBy;
    @Column
    private String  createDt;
    @Column
    private  String  apdateBy;
    @Column
    private String  updateDt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getLocalida() {
        return localida;
    }

    public void setLocalida(String localida) {
        this.localida = localida;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getTipoVia() {
        return tipoVia;
    }

    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    public String getNombreVia() {
        return nombreVia;
    }

    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    public int getNumeroVia() {
        return numeroVia;
    }

    public void setNumeroVia(int numeroVia) {
        this.numeroVia = numeroVia;
    }

    public String getPortal() {
        return portal;
    }

    public void setPortal(String portal) {
        this.portal = portal;
    }

    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getPuerta() {
        return puerta;
    }

    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getApdateBy() {
        return apdateBy;
    }

    public void setApdateBy(String apdateBy) {
        this.apdateBy = apdateBy;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }
}
