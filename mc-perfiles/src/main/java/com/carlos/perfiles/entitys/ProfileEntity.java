package com.carlos.perfiles.entitys;

import javax.persistence.*;


@Entity
@Table(name="sys_profile")
public class ProfileEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  int  id;
    @Column
    private  String  nombre;
    @Column
    private String  apellido;
    @Column
    private  int  idRol;
    @Column
    private  boolean  active;
    @Column
    private  String createBy;
    @Column
    private String  createDt;
    @Column
    private  String  apdateBy;
    @Column
    private String  updateDt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getApdateBy() {
        return apdateBy;
    }

    public void setApdateBy(String apdateBy) {
        this.apdateBy = apdateBy;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

}
