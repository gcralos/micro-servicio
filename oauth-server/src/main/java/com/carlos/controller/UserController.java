package com.carlos.controller;

import static com.carlos.utilitis.Constans.SIGN_UP_URL;
import static com.carlos.utilitis.Constans.USUARIO_ID;


import com.carlos.entity.PerfilEntity;
import com.carlos.entity.RoleEntity;
import com.carlos.model.UserDto;
import com.carlos.services.PerfilServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.carlos.entity.UserEntity;
import com.carlos.model.User;
import com.carlos.services.UserServices;
import org.modelmapper.ModelMapper;


@RestController
public class UserController {

    @Autowired
    UserServices userServices;
    @Autowired
    PerfilServices perfilServices;

    ModelMapper mapperModel = new ModelMapper();


    @PostMapping(SIGN_UP_URL)
    public   ResponseEntity<Object> getUsuario(@RequestBody  UserDto userDto) throws Exception {

       PerfilEntity perfilEntity = mapperModel.map(userDto.getPerfil(), PerfilEntity.class);
       perfilServices.add(perfilEntity);
       UserEntity userEntity = mapperModel.map(userDto.getUser(), UserEntity.class);
       RoleEntity roleEntity  =  mapperModel.map(userDto.getRol(),  RoleEntity.class);
       return userServices.add(userEntity,perfilEntity, roleEntity);
    }

    @PutMapping(USUARIO_ID)
    public ResponseEntity<Object> updateUsuario(@RequestBody User usuario,
                                                @PathVariable Long id) {
        UserEntity userEntity = mapperModel.map(usuario, UserEntity.class);
        return userServices.update(userEntity, id);
    }

    @DeleteMapping(USUARIO_ID)
    public void deleteUsuario(@PathVariable Long id) {
        userServices.delete(id);
    }

}
