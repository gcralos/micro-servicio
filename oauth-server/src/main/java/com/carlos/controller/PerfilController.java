package com.carlos.controller;

import com.carlos.entity.PerfilEntity;
import com.carlos.model.Perfil;
import com.carlos.services.PerfilServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.carlos.utilitis.Constans.PROFILE;
import static com.carlos.utilitis.Constans.PROFILE_ID;

@RestController
public class PerfilController {

    @Autowired
    PerfilServices perfilServices;
    ModelMapper mapperModel = new ModelMapper();


    @PostMapping(PROFILE)
    public ResponseEntity<Object> getUsuario(@RequestBody Perfil perfil) throws Exception {
        PerfilEntity perfilEntity = mapperModel.map(perfil, PerfilEntity.class);
        return perfilServices.add(perfilEntity);

    }

    @PutMapping(PROFILE_ID)
    public ResponseEntity<Object> updateUsuario(@RequestBody Perfil perfil,
                                                @PathVariable Long id) {
        PerfilEntity perfilEntity = mapperModel.map(perfil, PerfilEntity.class);
        return perfilServices.update(perfilEntity, id);
    }

    @DeleteMapping(PROFILE_ID)
    public void deleteUsuario(@PathVariable Long id) {
        perfilServices.delete(id);
    }

}

