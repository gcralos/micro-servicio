package com.carlos.entity;

import javax.persistence.*;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
@Table(name = "sys_user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NonNull
    @Column(length = 100, unique = true)
    private String userName;

    @NonNull
    @Column(length = 100)
    private String password;

    @NonNull
    @Column(length = 100)
    private String createBy;

    @NonNull
    @Column()
    @JsonFormat(pattern = "yyyy-mm-dd", shape = Shape.STRING)
    private String createDt;


    @Column(length = 100)
    private String updateBy;

    @Column()
    @JsonFormat(pattern = "yyyy-mm-dd", shape = Shape.STRING)
    private String updateDt;

    @JoinColumn( name="fk_user_profile_id")
    @OneToOne
    private PerfilEntity perfilEntity;

    @JoinColumn(name="fk_rol")
    @ManyToOne
    private RoleEntity  roleEntity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public PerfilEntity getPerfilEntity() {
        return perfilEntity;
    }

    public void setPerfilEntity(PerfilEntity perfilEntity) {
        this.perfilEntity = perfilEntity;
    }

    public RoleEntity getRoleEntity() {
        return roleEntity;
    }

    public void setRoleEntity(RoleEntity roleEntity) {
        this.roleEntity = roleEntity;
    }
}
