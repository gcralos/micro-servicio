package com.carlos.entity;

import javax.persistence.*;

@Entity
@Table( name="sys_roles")
public class RoleEntity {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private  Long  id;

    @Column(length = 20)
    private  String nombreRol;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }
}
