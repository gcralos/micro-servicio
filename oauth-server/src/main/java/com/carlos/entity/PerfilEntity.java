package com.carlos.entity;

import javax.persistence.*;

@Entity
@Table(name="sys_perfil")
public class PerfilEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long  id;
    @Column(length = 50)
    private  String  nombrePerson;
    @Column(length = 50)
    private  String  primerApellido;
    @Column(length = 60)
    private  String  nifNie;
    @Column(length = 30)
    private  String  tipoVia;
    @Column(length = 10)
    private  String nemero;
    @Column(length = 60)
    private  String  nombreVia;
    @Column(length =20)
    private  String codigoPostal;
    @Column(length = 60)
    private  String localida;
    @Column(length = 60)
    private  String  provincia;
    @Column(length = 60)
    private  String portal;
    @Column(length = 20)
    private  String bloque;
    @Column(length = 20)
    private  String  escalera;
    @Column(length = 20)
    private  String piso;
    @Column(length = 20)
    private  String  puerta;
    @Column(length = 20)
    private  String telefono;
    @Column(length = 20)
    private  String  movil;
    @Column(length = 20)
    private  String fax;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombrePerson() {
        return nombrePerson;
    }

    public void setNombrePerson(String nombrePerson) {
        this.nombrePerson = nombrePerson;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getNifNie() {
        return nifNie;
    }

    public void setNifNie(String nifNie) {
        this.nifNie = nifNie;
    }

    public String getTipoVia() {
        return tipoVia;
    }

    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    public String getNemero() {
        return nemero;
    }

    public void setNemero(String nemero) {
        this.nemero = nemero;
    }

    public String getNombreVia() {
        return nombreVia;
    }

    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getLocalida() {
        return localida;
    }

    public void setLocalida(String localida) {
        this.localida = localida;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPortal() {
        return portal;
    }

    public void setPortal(String portal) {
        this.portal = portal;
    }

    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    public String getEscalera() {
        return escalera;
    }

    public void setEscalera(String escalera) {
        this.escalera = escalera;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getPuerta() {
        return puerta;
    }

    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
}
