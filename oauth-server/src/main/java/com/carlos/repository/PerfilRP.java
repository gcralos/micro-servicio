package com.carlos.repository;

import com.carlos.entity.PerfilEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerfilRP extends JpaRepository<PerfilEntity,Long> {

}
