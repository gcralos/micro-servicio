package com.carlos.utilitis;

public class Constans {
    // Spring Security
    public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
    public static final String TOKEN_BEARER_PREFIX = "Bearer ";
    // JWT
    public static final String ISSUER_INFO = "https://www.autentia.com/";
    public static final String SUPER_SECRET_KEY = "C181AD68642A5FC41538130EEEE93D1A1B8AB7C83B8557CBFA2C3DA041E1A031";
    public static final long TOKEN_EXPIRATION_TIME = 86000;//864_000_000; // 10 day

    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 86000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    //END-PONIS
    public static final String  LOGIN  = "/login";
    public static final String SIGN_UP_URL = "/v1/api/signup";
    public static final String USUARIO_ID = "/v1/api/usuario/{id}";

    public static final String PROFILE = "/v1/api/perfil";
    public static final String PROFILE_ID = "/v1/api/perfil/{id}";

    //MESSAJE
    public  static  final  String   CREATE_MESSAJE= "Registro  ingresado  con exito";

}
