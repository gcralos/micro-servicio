package com.carlos.services;

import com.carlos.entity.PerfilEntity;
import com.carlos.repository.PerfilRP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PerfilServices {

    @Autowired
    private PerfilRP perfilRP;
    private PerfilEntity  perfilEntity;
    public PerfilEntity getId(Long id) {

        Optional<PerfilEntity> optional = perfilRP.findById(id);

        if (!optional.isPresent())
            return null;

        return optional.get();
    }

    public ResponseEntity<Object> add(PerfilEntity perfil) throws Exception {

        try {
            perfilEntity = perfilRP.save(perfil);
            return ResponseEntity.status(201).body("Registro  ingresado  con exito");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e);

        }

    }

    public ResponseEntity<Object> update(PerfilEntity perfil, Long id) {

        try {
            @SuppressWarnings("unused")
            Optional<PerfilEntity> optional = perfilRP.findById(id);
            perfil.setId(id);
            perfilRP.save(perfil);
            return ResponseEntity.status(201).body(perfil);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e);
        }

    }

    public void delete(Long id) {

        perfilRP.deleteById(id);
    }



}
