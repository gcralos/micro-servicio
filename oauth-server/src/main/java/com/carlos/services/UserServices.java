package com.carlos.services;


import java.util.Optional;


import com.carlos.entity.PerfilEntity;
import com.carlos.entity.RoleEntity;
import com.carlos.repository.PerfilRP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.carlos.entity.UserEntity;
import com.carlos.repository.UserRP;

import static com.carlos.utilitis.Constans.CREATE_MESSAJE;


@Service
public class UserServices {

    @Autowired
    private UserRP userRP;
    private UserEntity userModel;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserEntity getId(Long id) {

        Optional<UserEntity> optional = userRP.findById(id);

        if (!optional.isPresent())
            return null;

        return optional.get();
    }

    public ResponseEntity<Object> add(UserEntity user, PerfilEntity perfil, RoleEntity  rol) throws Exception {

            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setPerfilEntity(perfil);
            user.setRoleEntity(rol);
            userModel = userRP.save(user);
            return ResponseEntity.status(201).body(CREATE_MESSAJE);
    }

    public ResponseEntity<Object> update(UserEntity user, Long id) {

        try {
            @SuppressWarnings("unused")
            Optional<UserEntity> optional = userRP.findById(id);
            user.setId(id);
            userRP.save(user);
            return ResponseEntity.status(201).body(user);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e);
        }

    }

    public void delete(Long id) {

        userRP.deleteById(id);
    }

}
