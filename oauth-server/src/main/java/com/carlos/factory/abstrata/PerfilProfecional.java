package com.carlos.factory.abstrata;

public abstract class PerfilProfecional {
    protected  String  nombre;
    protected  String  apellido;
    protected  String  nieNif;
    protected  String  tipoVia;
    protected  String numero;
    protected  String  nombreVia;
    protected  String codigoPostal;
    protected  String localida;
    protected  String  provincia;
    protected  String portal;
    protected  String bloque;
    protected  String  escalera;
    protected  String piso;
    protected  String  puerta;
    protected  String telefono;
    protected  String  movil;
    protected  String fax;
    protected  String nombreEmpresa;
    protected  String actividaComercial;
    protected  String rol;
    protected  String cargo;

    public PerfilProfecional(String nombre, String apellido, String nieNif, String tipoVia, String numero, String nombreVia, String codigoPostal, String localida, String provincia, String portal, String bloque, String escalera, String piso, String puerta, String telefono, String movil, String fax, String nombreEmpresa, String actividaComercial, String rol, String cargo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nieNif = nieNif;
        this.tipoVia = tipoVia;
        this.numero = numero;
        this.nombreVia = nombreVia;
        this.codigoPostal = codigoPostal;
        this.localida = localida;
        this.provincia = provincia;
        this.portal = portal;
        this.bloque = bloque;
        this.escalera = escalera;
        this.piso = piso;
        this.puerta = puerta;
        this.telefono = telefono;
        this.movil = movil;
        this.fax = fax;
        this.nombreEmpresa = nombreEmpresa;
        this.actividaComercial = actividaComercial;
        this.rol = rol;
        this.cargo = cargo;
    }

    public  abstract  void  mostraPerfilProfecional();
}
