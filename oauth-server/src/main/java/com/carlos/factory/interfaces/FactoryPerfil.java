package com.carlos.factory.interfaces;

import com.carlos.factory.abstrata.PerfilPersonal;
import com.carlos.factory.abstrata.PerfilProfecional;

public interface FactoryPerfil  {
     PerfilProfecional createPerfilProfecional();
     PerfilPersonal creaPerfilPersonal();
}
